#include <iostream>
#include <time.h>
#include <signal.h>
#include <sys/siginfo.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/syspage.h>
#include <sys/neutrino.h>
#include <sched.h>
#include <cmath>
#include "cameraModule.h"

using namespace std;

#define PI 3.141592
//variables initiales pour les fonctions
#define INIT_NIV_BAT  100.
#define VITESSE_MAX_MS  5.
#define BAT_AUTONOMY_M  500.
#define TIME_TO_CHARGE_M 15.

#define TIME_PHOTO_S  4
#define TIME_TRANSM_S  10
#define PHOTO_FAIL_RATE  1

#define BAT_AUTONOMY_S  BAT_AUTONOMY_M*60.
#define TIME_TO_CHARGE_S  TIME_TO_CHARGE_M*60.

//Camera buffer size
#define BUFFER_PIC_NB 25

//Field waypoint tab length
#define WAYPOINTS_NB 28


// Etats de la mission
#define SCAN 0 //Scan the field (take pictures)
#define SENDING_PIC 1 //Sending the pictures
#define RTB 2 //Return above the base
#define RTW 3 //Return to work (last position before the drone came back to base)
#define CHARGE 4 //Charge at base
#define DISPLACEMENT_11 5 //Go to destination at 11m above the ground
#define DISPLACEMENT_5 6 //Go down to 5m
#define LANDING 7 //Land at base
#define FINISH 8 //The drone has finished its work
#define WAITING 9 //The drone wait


//mutex
pthread_mutex_t mut1 = PTHREAD_MUTEX_INITIALIZER; //current_pos et current_command
pthread_mutex_t mut2 = PTHREAD_MUTEX_INITIALIZER; //mission_state et destination
pthread_mutex_t mut3 = PTHREAD_MUTEX_INITIALIZER; //is_pic_taken
pthread_mutex_t mut4 = PTHREAD_MUTEX_INITIALIZER; //waypoint_photo
pthread_mutex_t mut5 = PTHREAD_MUTEX_INITIALIZER; //camera_available
//pthread_mutex_t mut6 = PTHREAD_MUTEX_INITIALIZER;
//pthread_mutex_t mut7 = PTHREAD_MUTEX_INITIALIZER;
//pthread_mutex_t mut8 = PTHREAD_MUTEX_INITIALIZER;
//pthread_mutex_t mut9 = PTHREAD_MUTEX_INITIALIZER;

//Sémaphores
sem_t sem1; // Arrived to dest
sem_t sem2; // Take photo
sem_t sem3; // SIMU Transmit photos
sem_t sem4; // SIMU Transmition confirmed
sem_t sem5; // camera available


typedef struct
{
    double x;
    double y;
    double z;
} position;

typedef struct
{
   double orientation;
   double horizontal_speed;
   double vertical_speed;
} command;

void pos2coord(position* position, coord_t* coord){
	coord->x=position->x;
	coord->y=position->y;
}

double dist2D(position* p1, position* p2) {
    double x1 = p1->x;
    double x2 = p2->x;
    double y1 = p1->y;
    double y2 = p2->y;
    return sqrt(pow((x1-x2),2) + pow((y1-y2),2));
}

double dist3D(position* p1, position* p2){
    double x1 = p1->x;
    double x2 = p2->x;
    double y1 = p1->y;
    double y2 = p2->y;
    double z1 = p1->z;
    double z2 = p2->z;
    return sqrt(pow((x1-x2),2)+pow((y1-y2),2)+pow((z1-z2),2));
}

double distZ(position* p, position* target){
    return target->z-p->z;
}

void set_position(position* destination, position* target){
    destination->x = target->x;
    destination->y = target->y;
    destination->z = target->z;
}



// Different variables used throughout the simulation
position* destination; //Destination of the drone
position* current_pos; //Current position of the drone
command* current_command; //Current navigation command send to the hardware
double current_orientation;
double last_orientation_command; //Last orientation command of the drone (used to calculate the current orientation command)
double target_speed; //Targeted horizontal speed (1m/s when scanning / 10m/s ortherwise)
position** waypoints; //Waypoints to explore the field
int waypoint_index; //Index in the waypoint tab of the current waypoint
int mission_state; //State of the mission controler
int last_mission_state; //State of the mission controlleur before an interruption (sending pictures)
position** base_acces_positions; //Positions used to reach the base (0 : base / 1 : upward)
position* return_position; //Position to reach after a low bat alarm an charge at base
int buffer; //Picture buffer (number of pictures stored)
int take_pic; //current picture taking command send to the hardware
int send_pic; //current transmission command sens to the hardware
bool need_resend; //Used to resent pictures if transmission failed (ex : low bat during transmission)
int is_pic_taken; //input of the picture confirmation from the hardware
bool last_pic_confirmed; //Used to be sure that the last picture was taken
bool camera_available; // Be sure that the camera is available to take a picture
position* pos_last_picture; //Position of the drone when the last picture was taken
double battery_level; //battery level
double current_hspeed; // vitesse horizontale actuelle
double current_vspeed; // vitesse verticale actuelle
bool charge_battery; // consigne de charge de la batterie
bool waypoint_photo; // Demande de prise de photo à un waypoint

// Camera
PathMap camera;
coord_t* coord_photo = new coord_t;


void *Displacement_battery_simulation(void *arg){
	bool low_bat = false;
	double delta_t;
	struct timespec last_time;
	struct timespec current_time;
	clock_gettime(CLOCK_REALTIME, &last_time);
	while(1){
		pthread_mutex_lock(&mut1);
		clock_gettime(CLOCK_REALTIME, &current_time);
		delta_t = (current_time.tv_sec+0.000000001*current_time.tv_nsec-last_time.tv_sec-0.000000001*last_time.tv_nsec);
		current_hspeed = min(VITESSE_MAX_MS,current_command->horizontal_speed);
		current_vspeed = current_command->vertical_speed;
		current_orientation = current_command->orientation;
		current_pos->x += sin(current_orientation*PI/180.)*current_hspeed*delta_t;
		current_pos->y += cos(current_orientation*PI/180.)*current_hspeed*delta_t;
		current_pos->z = max(0.,current_pos->z+current_vspeed*delta_t);
		if (charge_battery){
			battery_level += (80./(TIME_TO_CHARGE_S))*delta_t;
			if (battery_level>=100){
				raise(SIGUSR2);
				low_bat=false;
			}
		}
		else{
			battery_level -= (100./BAT_AUTONOMY_M)*(current_vspeed+current_hspeed)*delta_t;
		}
		battery_level = max(0.,battery_level);
		battery_level = min(100.,battery_level);
		if(battery_level<=10 && !low_bat){
			raise(SIGUSR1);
			low_bat = true;
		}
		last_time = current_time;
		pthread_mutex_unlock(&mut1);
		delay(10);
	}
	return(NULL);
}

void *Camera_simulation(void *arg){
	while(1){
		sem_wait(&sem2);
		pthread_mutex_lock(&mut3);
		is_pic_taken = 0;
		pthread_mutex_unlock(&mut3);
		pthread_mutex_lock(&mut1);
		pos2coord(current_pos,coord_photo);
		pthread_mutex_unlock(&mut1);
		camera.takePhoto(*coord_photo);
		pthread_mutex_lock(&mut3);
		is_pic_taken = 1;
		pthread_mutex_unlock(&mut3);
	}
}

void *Transmission_simulation(void *arg){
	while(1){
		sem_wait(&sem3);
		camera.transmitPhotos();
		sem_post(&sem4);
	}
}

void *Display(void *arg){
	while(1){
		pthread_mutex_lock(&mut1);
		pthread_mutex_lock(&mut2);
		switch (mission_state){
			case SCAN :
				printf("State : SCAN\n");
				break;
			case SENDING_PIC :
				printf("State : SENDING_PIC\n");
				break;
		 	case RTB :
		 		printf("State : RTB\n");
		 		break;
		 	case RTW :
		 		printf("State : RTW\n");
		 		break;
		 	case CHARGE :
		 		printf("State : CHARGE\n");
		 		break;
		 	case DISPLACEMENT_11 :
		 		printf("State : DISPLACEMENT_11\n");
		 		break;
		 	case DISPLACEMENT_5 :
		 		printf("State : DISPLACEMENT_5\n");
		 		break;
 			case LANDING :
 				printf("State : LANDING\n");
 				break;
 			case FINISH :
 				printf("State : FINISH\n");
 				break;
 			case WAITING :
 				printf("State : WAITING\n");
 				break;
		}
		printf("x : %lf / y : %lf / z : %lf\n",current_pos->x,current_pos->y,current_pos->z);
		printf("orientation : %lf\n",current_orientation);
		printf("h_speed : %lf / v_speed : %lf\n",current_hspeed,current_vspeed);
		printf("battery level : %lf\n",battery_level);
		printf("buffer : %i\n\n",buffer);
		pthread_mutex_unlock(&mut1);
		pthread_mutex_unlock(&mut2);
		delay(1000);
	}
	return(NULL);
}


//Fonction du contrôleur navigation
void * nav_ctrl_func(void* arg) {
	while(1){
		pthread_mutex_lock(&mut1);
		pthread_mutex_lock(&mut2);

		if (mission_state == WAITING){
			current_command->horizontal_speed =0;
			current_command->vertical_speed = 0;
		}
		else if (mission_state==SENDING_PIC && last_mission_state != FINISH){
			current_command->vertical_speed=0;
			if (dist2D(pos_last_picture,current_pos)>5.05){
					current_command->orientation = atan2(destination->x-current_pos->x,destination->y-current_pos->y)*180./PI;
					current_command->orientation = current_command->orientation+180.;
					current_command->horizontal_speed=0.1;
			}
			else{
					current_command->horizontal_speed=0;
			}

		}
		else if (mission_state != CHARGE && mission_state!=FINISH){

			current_command->orientation = atan2(destination->x-current_pos->x,destination->y-current_pos->y)*180./PI;

			if (abs(current_command->orientation+360.-last_orientation_command)<abs(current_command->orientation-last_orientation_command)){
				current_command->orientation = current_command->orientation +360.;
			}

			last_orientation_command = current_command->orientation;

			if (dist2D(current_pos,destination)>0.05){
				current_command->horizontal_speed = min(target_speed,dist2D(current_pos,destination));
			}
			else{
				current_command->horizontal_speed = 0;
			}

			if (abs(distZ(current_pos, destination))>0.05){
				if(distZ(current_pos, destination)>0){
					current_command->vertical_speed = min(1.,distZ(current_pos, destination));
				}
				else{
					current_command->vertical_speed = max(-1.,distZ(current_pos, destination));
				}
			}
			else{
				current_command->vertical_speed = 0;
			}
		}

		pthread_mutex_unlock(&mut1);

		if (mission_state != CHARGE && mission_state != SENDING_PIC && mission_state!=FINISH && mission_state!=WAITING){
			//We arrived to the destination
			if (dist3D(current_pos,destination) < 0.1) {
				printf("Arrived at destination\n");
				current_command->horizontal_speed=0;
				current_command->vertical_speed=0;
				sem_post(&sem1);
			}
		}
		pthread_mutex_unlock(&mut2);
		delay(100);
	}
}

//Reached destination handler
void* arrived_to_dest(void*arg) {
	while(1){
		sem_wait(&sem1);
		pthread_mutex_lock(&mut2);
		switch (mission_state){
		    case RTW:
		        set_position(destination,return_position);
		        destination->z=11;
		        mission_state = DISPLACEMENT_11;
		        target_speed = 10;
		        //printf("DISPLACEMENT_11\n");
		        break;

		    case DISPLACEMENT_11:
		        destination->z=5;
		        mission_state = DISPLACEMENT_5;
		        //printf("DISPLACEMENT_5\n");
		        break;

		    case DISPLACEMENT_5:
		        set_position(destination,waypoints[waypoint_index]);
		        mission_state=SCAN;
		        target_speed = 1;
		        //printf("SCAN\n");
		        break;

		    case SCAN:
		    	pthread_mutex_lock(&mut5);
		    	if (!camera_available){
		    		pthread_mutex_unlock(&mut5);
		    		printf("\nwaiting camera\n\n");
		    		pthread_mutex_lock(&mut2);
		    		mission_state = WAITING;
		    		pthread_mutex_unlock(&mut2);
		    		sem_wait(&sem5);
		    	}
		    	else{
		    		pthread_mutex_unlock(&mut5);
		    	}
		    	pthread_mutex_lock(&mut2);
		    	if (mission_state == WAITING){
		    		mission_state = SCAN;
		    	}
	    		pthread_mutex_unlock(&mut2);
		    	pthread_mutex_lock(&mut4);
		    	waypoint_photo=true;
		    	pthread_mutex_unlock(&mut4);
		        waypoint_index++;
		        if (waypoint_index < WAYPOINTS_NB) {
		             //the Mission controler gives the new point to the Nav controler
		            set_position(destination,waypoints[waypoint_index]);
		            printf("Next waypoint\n");
		        }
		        else {
		            //Retour à la base
		            printf("All points have been reached\n");
		            set_position(destination,base_acces_positions[1]);
		            mission_state = RTB;
		            target_speed = 10;
		            //printf("RTB\n");
		        }
		        break;

		    case RTB:
		        set_position(destination,base_acces_positions[0]);
		        mission_state = LANDING;
		        //printf("LANDING\n");
		        break;

		    case LANDING:
		        //printf("CHARGE\n");
		        charge_battery = true;

		        if (waypoint_index < WAYPOINTS_NB) {
		            if(need_resend){
		                last_mission_state = CHARGE;
		                mission_state = SENDING_PIC;
		                send_pic = 1;
		                printf("Resend\n");
		            }
		            else{
		                mission_state = CHARGE;
		            }
		        }
		        else {
		          	mission_state = FINISH;
		          	target_speed = 0;
		          	//in case pictures are in the buffer at the end of the mission
		          	if (buffer > 0) {
                  last_mission_state = FINISH;
                  mission_state = SENDING_PIC;
		            	send_pic = 1;
                  sem_post(&sem3);

		            	printf("Sending last pictures remaining\n");
		          	}
		        }

		}
		pthread_mutex_unlock(&mut2);
	}
}


//Fonction du contrôleur de caméra
void *cam_ctrl_func(void* arg) {
	struct timespec last_shot_time;
	struct timespec current_time;
	double shooting_time;

	while(1){
		clock_gettime(CLOCK_REALTIME, &current_time);
		shooting_time = (current_time.tv_sec+0.000000001*current_time.tv_nsec-last_shot_time.tv_sec-0.000000001*last_shot_time.tv_nsec);

		if (!camera_available || !last_pic_confirmed){
    		pthread_mutex_lock(&mut3);
	        if(is_pic_taken==1){
	        	pthread_mutex_lock(&mut5);
	        	camera_available = true;
	        	pthread_mutex_unlock(&mut5);
	        	pthread_mutex_lock(&mut2);
	        	if (mission_state==WAITING){
	        		pthread_mutex_unlock(&mut2);
  	        		sem_post(&sem5);
	        	}
	        	else{
	        		pthread_mutex_unlock(&mut2);
	        	}
	            buffer++;
	            last_pic_confirmed=true;
	            if (buffer == BUFFER_PIC_NB){
	            	pthread_mutex_lock(&mut2);
	                last_mission_state = mission_state;
	                mission_state=SENDING_PIC;
	                printf("SENDING_PIC\n");
	                pthread_mutex_unlock(&mut2);
	                //send_pic = 1;
	                sem_post(&sem3);
            	}
        	}
        	else if (shooting_time > 4.5){
        		printf("Error : picture not taken\n");
        		pthread_mutex_lock(&mut5);
        		camera_available = true;
        		pthread_mutex_unlock(&mut5);
        		if (mission_state==WAITING){
	        		pthread_mutex_unlock(&mut2);
	        		sem_post(&sem5);
	        	}
	        	else{
	        		pthread_mutex_unlock(&mut2);
	        	}
        	}
        	pthread_mutex_unlock(&mut3);
    	}
	    pthread_mutex_lock(&mut2);
	    if(mission_state == SCAN){
	        pthread_mutex_unlock(&mut2);
	        pthread_mutex_lock(&mut4);
	        if (waypoint_photo){
	        	pthread_mutex_unlock(&mut4);
	        	printf("Picture at waypoint\n\n");
	        	//take pic
	            clock_gettime(CLOCK_REALTIME, &last_shot_time);
	            pthread_mutex_lock(&mut1);
	            set_position(pos_last_picture,current_pos);
	            pthread_mutex_unlock(&mut1);
	            last_pic_confirmed=false;
	            waypoint_photo=false;
	            camera_available=false;
	            sem_post(&sem2);
	        }
	        else{
	        	pthread_mutex_unlock(&mut4);
	        	pthread_mutex_lock(&mut1);
	        	if (dist2D(pos_last_picture,current_pos)>4.9){
	            	printf("Picture distance : %lf\n\n",dist2D(pos_last_picture,current_pos));
		            //take pic
		            set_position(pos_last_picture,current_pos);
		            last_pic_confirmed=false;
		            camera_available = false;
		            clock_gettime(CLOCK_REALTIME, &last_shot_time);
		            sem_post(&sem2);
		        }
	        	pthread_mutex_unlock(&mut1);
	    	}
	    }
	    else{
	    	pthread_mutex_unlock(&mut2);
	    }

		delay(100);
	}
}

//Low battery alarm handler
void batt_low_alarm(int signum){
	printf("LOW_BAT\n");
	pthread_mutex_lock(&mut1);
	pthread_mutex_lock(&mut2);
    if (mission_state==SENDING_PIC){
        need_resend=true;
    }
    set_position(return_position,current_pos);
    set_position(destination,base_acces_positions[1]);
    target_speed = 10;
    mission_state = RTB;
    pthread_mutex_unlock(&mut1);
    pthread_mutex_unlock(&mut2);
}



//Full battery alarm handler
void batt_full_alarm(int signum){
	printf("FULL_BAT\n");
    charge_battery = false;
	pthread_mutex_lock(&mut2);
    if (mission_state == CHARGE){
        set_position(destination,base_acces_positions[1]);
        mission_state = RTW;
    }
    pthread_mutex_unlock(&mut2);
}


//Transmission confirmed handler
void *pic_sent(void* arg) {
    //send_pic = 0;
    while(1){
    	sem_wait(&sem4);
    	pthread_mutex_lock(&mut2);
    	if(mission_state==SENDING_PIC){
        buffer = 0;
        if (last_mission_state == WAITING){
        	mission_state = SCAN;
        }
        else{
        	mission_state=last_mission_state;
        }
        printf("PICS SENT\n");
    	}
    	if(last_mission_state==FINISH) {
          mission_state = FINISH;
      		buffer = 0;
      		printf("Last pics sent\n");
    	}
    	pthread_mutex_unlock(&mut2);
    }
}

void init_tasks(void) {
	pthread_t tid[10];
	pthread_attr_t attrib;
	int i;
	// ordonnancement FIFO
	struct sched_param mySchedParam;
	pthread_attr_setschedpolicy (&attrib, SCHED_FIFO);

	//Init sémaphores
	sem_init(&sem1, 0, 0 );
	sem_init(&sem2, 0, 0 );
	sem_init(&sem3, 0, 0 );
	sem_init(&sem4, 0, 0 );
	sem_init(&sem5, 0, 0 );


	//Init handler signaux
	signal(SIGUSR1,batt_low_alarm);
	signal(SIGUSR2,batt_full_alarm);

	//Création des threads
	pthread_attr_init (&attrib);

	mySchedParam.sched_priority = 20;
	pthread_attr_setschedparam(&attrib, &mySchedParam);
	if ( pthread_create(&tid[0], &attrib, Displacement_battery_simulation, NULL ) < 0)
		printf("taskSpawn Displacement_battery_simulation failed!\n");

	mySchedParam.sched_priority = 1;
	pthread_attr_setschedparam(&attrib, &mySchedParam);
	if ( pthread_create(&tid[1], &attrib, Display, NULL ) < 0)
		printf("taskSpawn Display failed!\n");

	mySchedParam.sched_priority = 11;
  	pthread_attr_setschedparam(&attrib, &mySchedParam);
  	if ( pthread_create(&tid[2], &attrib, nav_ctrl_func, NULL ) < 0)
  		printf("Echec thread nav_ctrl_func\n");

	mySchedParam.sched_priority = 11;
	pthread_attr_setschedparam(&attrib, &mySchedParam);
	if ( pthread_create(&tid[3], &attrib, arrived_to_dest, NULL ) < 0)
		printf("Echec thread arrived_to_dest\n");


	mySchedParam.sched_priority = 16;
  	pthread_attr_setschedparam(&attrib, &mySchedParam);
  	if ( pthread_create(&tid[4], &attrib, pic_sent, NULL ) < 0)
  		printf("Echec thread pic_sent\n");

	mySchedParam.sched_priority = 12;
  	pthread_attr_setschedparam(&attrib, &mySchedParam);
 	if ( pthread_create(&tid[5], &attrib, cam_ctrl_func, NULL ) < 0)
  		printf("Echec thread cam_ctrl_func\n");

	mySchedParam.sched_priority = 15;
  	pthread_attr_setschedparam(&attrib, &mySchedParam);
  	if ( pthread_create(&tid[6], &attrib, Transmission_simulation, NULL ) < 0)
  		printf("Echec thread transmission\n");

	mySchedParam.sched_priority = 15;
 	 pthread_attr_setschedparam(&attrib, &mySchedParam);
  	if ( pthread_create(&tid[7], &attrib, Camera_simulation, NULL ) < 0)
  		printf("Echec thread camera\n");


	for (i=0; i < 8; i++)
		pthread_join(tid[i], NULL); //attendre la fin de l'ensemble des thread

}





int main() {

	//Battery
	charge_battery = false;
	battery_level = INIT_NIV_BAT;

	//Init Camera buffer
    buffer = 0;

    //Init picture variables
    last_pic_confirmed = true;
    need_resend = false;
	pos_last_picture = new position;
	camera_available = true;
	waypoint_photo = false;

    //Init command
    current_command = new command;
    last_orientation_command =0;

    //Init speed
    target_speed = 1;

    //Init state
    mission_state=RTW;

    //Start position
    current_pos = new position;
    current_pos->x=0;
    current_pos->y=0;
    current_pos->z=0;

    //Base acces
    base_acces_positions = new position*[2];
    position* base = new position;
    position* upward = new position;
    base->x = 0;
    base->y = 0;
    base->z = 0;
    upward->x = 0;
    upward->y = 0;
    upward->z = 11;
    base_acces_positions[0]=base;
    base_acces_positions[1]=upward;

    //Init destination
    destination = new position;
    set_position(destination,upward);

    // Field Waypoints
    waypoint_index = 1;

    position* pos1 = new position;
    pos1->x = 20.5;
    pos1->y = 77.5;
    pos1->z = 5;

    position* pos2 = new position;
    pos2->x = 43.5;
    pos2->y = 77.5;
    pos2->z = 5;

    position* pos3 = new position;
    pos3->x = 43.5;
    pos3->y = 72.5;
    pos3->z = 5;

    position* pos4 = new position;
    pos4->x = 20.5;
    pos4->y = 72.5;
    pos4->z = 5;

    position* pos5 = new position;
    pos5->x = 20.5;
    pos5->y = 67.5;
    pos5->z = 5;

    position* pos6 = new position;
    pos6->x = 43.5;
    pos6->y = 67.5;
    pos6->z = 5;

    position* pos7 = new position;
    pos7->x = 43.5;
    pos7->y = 62.5;
    pos7->z = 5;

    position* pos8 = new position;
    pos8->x = 20.5;
    pos8->y = 62.5;
    pos8->z = 5;

    position* pos9 = new position;
    pos9->x = 20.5;
    pos9->y = 57.5;
    pos9->z = 5;

    position* pos10 = new position;
    pos10->x = 43.5;
    pos10->y = 57.5;
    pos10->z = 5;

    position* pos11 = new position;
    pos11->x = 43.5;
    pos11->y = 52.5;
    pos11->z = 5;

    position* pos12 = new position;
    pos12->x = 20.5;
    pos12->y = 52.5;
    pos12->z = 5;

    position* pos13 = new position;
    pos13->x = 12.5;
    pos13->y = 47.5;
    pos13->z = 5;

    position* pos14 = new position;
    pos14->x = 57.5;
    pos14->y = 47.5;
    pos14->z = 5;

    position* pos15 = new position;
    pos15->x = 57.5;
    pos15->y = 42.5;
    pos15->z = 5;

    position* pos16 = new position;
    pos16->x = 12.5;
    pos16->y = 42.5;
    pos16->z = 5;

    position* pos17 = new position;
    pos17->x = 12.5;
    pos17->y = 37.5;
    pos17->z = 5;

    position* pos18 = new position;
    pos18->x = 57.5;
    pos18->y = 37.5;
    pos18->z = 5;

    position* pos19 = new position;
    pos19->x = 57.5;
    pos19->y = 32.5;
    pos19->z = 5;

    position* pos20 = new position;
    pos20->x = 12.5;
    pos20->y = 32.5;
    pos20->z = 5;

    position* pos21 = new position;
    pos21->x = 12.5;
    pos21->y = 27.5;
    pos21->z = 5;

    position* pos22 = new position;
    pos22->x = 57.5;
    pos22->y = 27.5;
    pos22->z = 5;

    position* pos23 = new position;
    pos23->x = 57.5;
    pos23->y = 22.5;
    pos23->z = 5;

    position* pos24 = new position;
    pos24->x = 12.5;
    pos24->y = 22.5;
    pos24->z = 5;

    position* pos25 = new position;
    pos25->x = 12.5;
    pos25->y = 17.5;
    pos25->z = 5;

    position* pos26 = new position;
    pos26->x = 57.5;
    pos26->y = 17.5;
    pos26->z = 5;

    position* pos27 = new position;
    pos27->x = 57.5;
    pos27->y = 12.5;
    pos27->z = 5;

    position* pos28 = new position;
    pos28->x = 12.5;
    pos28->y = 12.5;
    pos28->z = 5;

    waypoints = new position*[WAYPOINTS_NB];

    waypoints[0]=pos1;
    waypoints[1]=pos2;
    waypoints[2]=pos3;
    waypoints[3]=pos4;
    waypoints[4]=pos5;
    waypoints[5]=pos6;
    waypoints[6]=pos7;
    waypoints[7]=pos8;
    waypoints[8]=pos9;
    waypoints[9]=pos10;
    waypoints[10]=pos11;
    waypoints[11]=pos12;
    waypoints[12]=pos13;
    waypoints[13]=pos14;
    waypoints[14]=pos15;
    waypoints[15]=pos16;
    waypoints[16]=pos17;
    waypoints[17]=pos18;
    waypoints[18]=pos19;
    waypoints[19]=pos20;
    waypoints[20]=pos21;
    waypoints[21]=pos22;
    waypoints[22]=pos23;
    waypoints[23]=pos24;
    waypoints[24]=pos25;
    waypoints[25]=pos26;
    waypoints[26]=pos27;
    waypoints[27]=pos28;

    return_position = new position;
    set_position(return_position,pos1);

	init_tasks();

	return 0;
}
